import pandas as pd
import random as rd

df = pd.read_csv("Seasons_Stats.csv")

# Filtre sur le nombre de match démarrés
filtre_GS = df.GS >= 70
df_GS = df[filtre_GS]

# Filtre sur les années prises en compte
filtre_2k = df_GS.Year >= 2000
df_2k = df_GS[filtre_2k]

nb_row = df_2k.shape[0]

filtre_test = list(rd.sample(range(nb_row), 100))
filtre_base = [k for k in range(nb_row) if not k in filtre_test]

df_test = df_2k.iloc[filtre_test]
df_base = df_2k.iloc[filtre_base]

df_test_pos = df_test["Pos"]
df_test_pos.to_csv("test_labels.csv", index=False)

df_test_stats = df_test.loc[:, "PER":]
df_test_stats = df_test_stats.loc[:, df_test_stats.columns != "blanl"]
df_test_stats = df_test_stats.loc[:, df_test_stats.columns != "blank2"]
df_test_stats.to_csv("test_stats.csv", index=False)

df_base_pos = df_base["Pos"]
df_base_pos.to_csv("train_labels.csv", index=False)

df_base_stats = df_base.loc[:, "PER":]
df_base_stats = df_base_stats.loc[:, df_base_stats.columns != "blanl"]
df_base_stats = df_base_stats.loc[:, df_base_stats.columns != "blank2"]
df_base_stats.to_csv("train_stats.csv", index=False)