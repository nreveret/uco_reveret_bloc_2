import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler

df_values = pd.read_csv("train_stats.csv")
df_labels = pd.read_csv("train_labels.csv", names = ["Position"])

# Separating out the features
x = df_values.values

# Separating out the target
y = df_labels.values

# Standardizing the features
x = StandardScaler().fit_transform(x)
df_x = pd.DataFrame(data = x)

from sklearn.decomposition import PCA

pca = PCA(n_components=2)

principalComponents = pca.fit_transform(x)

principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])


print("Pourcentage d'explication" , pca.explained_variance_ratio_)
#principalDf.to_excel('PCA_leaf.xlsx')

i = np.identity(x.shape[1])

coeff = pca.transform(i)
df_coeff = pd.DataFrame(data = coeff)

finalDf = pd.concat([principalDf, df_labels[["Position"]]], axis = 1)

fig = plt.figure(figsize = (12,8))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('1ère composante principale', fontsize = 15)
ax.set_ylabel('2ème composante principale', fontsize = 15)
ax.set_title('Représentation des Joueurs', fontsize = 20)
targets = pd.unique(y[:,0])
targets = ['CB', 'CM', 'F', 'LB', 'LW', 'RB', 'RW', 'ST']

for target in targets :
    indicesToKeep = finalDf["Position"] == target
    ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
               , finalDf.loc[indicesToKeep, 'principal component 2']
               , s = 50)
ax.legend(targets)
ax.grid()

fig.savefig("acp.png")