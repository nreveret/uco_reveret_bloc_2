import pandas as pd

"""
df = pd.read_csv("data.csv", delimiter=",")

for index, row in df.iterrows():
    if row["Value"] in ["", "€0"] :
        df.loc[index, "Value"] = "0"
    elif row["Value"][-1] == "M" :
        df.loc[index, "Value"] = float(row["Value"][1:-1]) * 1000
    else :
        df.loc[index, "Value"] = float(row["Value"][1:-1])

df.to_csv("data2.csv")
"""

import random as rd

df = pd.read_csv("data2.csv")

filtre_Value = df.Value >= 500
df_Value = df[filtre_Value]
df_Value.to_csv("data_value.csv", index = False)

nb_row = df_Value.shape[0]

filtre_test = list(rd.sample(range(nb_row), 1000))
filtre_base = [k for k in range(nb_row) if not k in filtre_test]

df_test = df_Value.iloc[filtre_test]
df_base = df_Value.iloc[filtre_base]

df_test_pos = df_test["Position"]
df_test_pos.to_csv("test_pos.csv", index=False)

df_test_stats = df_test.loc[:, "Crossing":"GKReflexes"]
df_test_stats.to_csv("test_stats.csv", index=False)

df_base_pos = df_base["Position"]
df_base_pos.to_csv("base_pos.csv", index=False)

df_base_stats = df_base.loc[:, "Crossing":"GKReflexes"]
df_base_stats.to_csv("base_stats.csv", index=False)
