import pandas as pd
import random as rd

nb_test = 500

df = pd.read_csv("data2.csv")

def valeur(x):
    dico_positions = {'GK': "GK",
                      'RF': "RW",
                      'ST': "ST",
                      'LW': "LW",
                      'RCM': "RW",
                      'LF': "LW",
                      'RS': "ST",
                      'RCB': "CB",
                      'LCM': "LW",
                      'CB': "CB",
                      'LDM': "LW",
                      'CAM': "CM",
                      'CDM': "CM",
                      'LS': "ST",
                      'LCB': "CB",
                      'RM': "RW",
                      'LAM': "CM",
                      'LM': "CM",
                      'LB': "LB",
                      'RDM': "RW",
                      'RW': "RW",
                      'CM': "CM",
                      'RB': "RB",
                      'RAM': "RW",
                      'CF': "F",
                      'RWB': "RW",
                      'LWB': "LW"}

    dico_positions = {'GK': "GK",
                      'RF': "W",
                      'ST': "ST",
                      'LW': "W",
                      'RCM': "W",
                      'LF': "W",
                      'RS': "ST",
                      'RCB': "CB",
                      'LCM': "W",
                      'CB': "CB",
                      'LDM': "W",
                      'CAM': "CM",
                      'CDM': "CM",
                      'LS': "ST",
                      'LCB': "CB",
                      'RM': "W",
                      'LAM': "CM",
                      'LM': "CM",
                      'LB': "B",
                      'RDM': "W",
                      'RW': "W",
                      'CM': "CM",
                      'RB': "B",
                      'RAM': "W",
                      'CF': "F",
                      'RWB': "W",
                      'LWB': "W"}

    return dico_positions[x["Position"]]

df['Position_Simple'] = df.apply(lambda row: valeur(row), axis=1)


filtre_Value = df.Value >= 1000
df_Value = df[filtre_Value]
df_Value.to_csv("data_value.csv", index=False)

filtre_Joueur_Champ = df.Position != "GK"
df_Joueur_Champ = df_Value.loc[filtre_Joueur_Champ]
#df_Joueur_Champ.to_csv("data_joueur_champ.csv", index=False)

nb_row = df_Joueur_Champ.shape[0]

filtre_test = list(rd.sample(range(nb_row), nb_test))
filtre_base = [k for k in range(nb_row) if not k in filtre_test]

df_test = df_Joueur_Champ.iloc[filtre_test]
df_base = df_Joueur_Champ.iloc[filtre_base]

df_test_pos = df_test["Position_Simple"]
df_test_pos.to_csv("test_labels.csv", index=False, header=False)

df_test_stats = df_test.loc[:, "Crossing":"SlidingTackle"]
df_test_stats.to_csv("test_stats.csv", index=False)

df_base_pos = df_base["Position_Simple"]
df_base_pos.to_csv("train_labels.csv", index=False, header=False)

df_base_stats = df_base.loc[:, "Crossing":"SlidingTackle"]
df_base_stats.to_csv("train_stats.csv", index=False)
